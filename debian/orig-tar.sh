#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=graphy-$2
TAR=graphy_$2.orig.tar.gz

# Repack upstream source to tar.gz and clean it
tar jxf $3
mv graphy_$2 $DIR
GZIP=--best tar -cz --owner root --group root --mode a+rX \
    -f $TAR -X debian/orig-tar.excludes $DIR
rm -rf $DIR $3
